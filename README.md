# Arch Linux Package Mirror on IPFS

**WARNING: Currently non-operational.** The `all` directory has gotten too large for a single IPFS directory and I haven't implmemented sharding (mostly because there are no docs and I haven't reverse-engineered it myself yet). Ideally IPFS would provide an API to create a directory and manage sharding/size calculation automatically but that isn't currently the case. 🤷

## How to use:

At the following lines to `/etc/pacman.d/mirrorlist`.

Tip: You can use multiple gateways to work around any downtime.

### Local Daemon

```ini
Server = http://localhost:8080/ipns/arch.kevincox.ca/all/$repo/os/$arch
```

This is for running the daemon on the same machine. Another good option is running a gateway on a common server. Just replace `localhost:8080` with the gateway address.

### Public Gateways

```ini
Server = https://ipfs.io/ipns/arch.kevincox.ca/all/$repo/os/$arch

# Note: Not recommended because it doesn't support IPFS Pubsub which leads to timeouts.
# https://github.com/ipfs/go-ipfs/blob/master/docs/experimental-features.md#ipns-pubsub
Server = https://cloudflare-ipfs.com/ipns/arch.kevincox.ca/all/$repo/os/$arch
```

### Raw IPNS

The archive is also available at /ipns/12D3KooWPBW6yWfaJcChuH5iZCzDYJDMaDMgzoRL3h7hqhyt3aUe. You may use this for stronger decentralization and better authenticity.

## Available Repos

Note: I am personally pinning the latest couple of snapshots. I will not be pinning old packages indefinitely.

### all

This is the recommended directory to point pacman at. It has references to all packages since I started this mirror.

**Warning:** While I will keep all packages listed in this directory I will not be pinning old packages. You may find that older packages are unavailable.

### latest

This is a mirror of the live status of `rsync.archlinux.org`. It will have all of the latest files but little to no history. This means that if you haven't updated in a while and try to install a new package you may get a 404.

This may be slightly faster than `all` due to the smaller directory sizes.

### snapshots/0000-00-00T00:00:00

This directory contains historical snapshots of the package repository. You can use these directories to sync back to a specific point in time.

**Warning:** I only promise to pin the latest version (this is the same as `latest`). If you want older versions to stay around be sure to pin them yourself.

## FAQ

### Why should I trust you?

You don't have to. Because arch packages are signed there isn't much harm that can be done by a mirror.

### Have you heard about https://github.com/victorb/arch-mirror?

Yes, however I thought it would be interesting to include more history. victorb's mirror is equivalent to the `latest` directory of this mirror.

## Technical Notes

The `latest` folder is about 40GiB (as of March 2020) however due to IPFS gateway limitations I have removed the symlinks that are used by regular mirrors. This means that a simple check-out is more than twice the size.
