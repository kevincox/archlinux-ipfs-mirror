{ pkgs ? import <nixpkgs> {} }: with pkgs; let
in rec {
	archlinux-ipfs-mirror = rustPlatform.buildRustPackage {
		name = "archlinux-ipfs-mirror";
		cargoSha256 = null;
		src = builtins.filterSource (name: type:
			(lib.hasPrefix (toString ./src) name) ||
			(lib.hasPrefix (toString ./tests) name) ||
			(lib.hasPrefix (toString ./Cargo) name)) ./.;
		doCheck = true;
		buildInputs = [
			openssl
			pkgconfig
		];
		RSYNC_PATH = "${rsync}/bin/rsync";
	};
}
