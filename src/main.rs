use futures::stream::StreamExt;
use itertools::Itertools;
use std::convert::TryInto;

const USAGE: &'static str = "
Usage: archlinux-ipfs-mirror [options]

Options:
	--base=<ipfs-path>  The path to update. [default: /ipns/12D3KooWPBW6yWfaJcChuH5iZCzDYJDMaDMgzoRL3h7hqhyt3aUe]
	--debug  Set some flags to make debugging more pleasant.
	--ipns-key=<name>  The key to use for publishing [default: archlinux-ipfs-mirror]
	--mirror-rsync=<url>  The URL to mirror. [default: rsync://mirror2.evolution-host.com/archlinux/]
	--mirror-url=<url>  The rsync path to the mirror. [default: https://mirror2.evolution-host.com/archlinux/]
	--pin-count=<num>  Don't unpin the latest <num> snapshots. If set to 0 nothing will be unpinned. WARNING: If non-zero all unrelated pins in the IPFS instance will be removed. [default: 0]
	--pin-time=<duration>  Don't unpin snapshots more recent than <duration> ago. Example: 1year or 1month. See https://docs.rs/humantime/2/humantime/fn.parse_duration.html for syntax.
	--work-dir=<path>  The directory in which to store files. [default: archlinux-ipfs-mirror]

	--help  Show this message.
";

#[derive(serde::Deserialize)]
struct Args {
	flag_base: String,
	flag_debug: bool,
	flag_ipns_key: String,
	flag_mirror_rsync: String,
	flag_mirror_url: url::Url,
	flag_pin_count: usize,
    #[serde(with = "humantime_serde")]
	flag_pin_time: Option<std::time::Duration>,
	flag_work_dir: std::path::PathBuf,
}

#[derive(Debug)]
struct File {
	/// Unix timestamp.
	last_modified: chrono::DateTime<chrono::Utc>,
	size: u64,
}

#[derive(Debug,serde::Deserialize,serde::Serialize)]
struct CacheEntry<'a> {
	link: IpfsLink,

	#[serde(with="serde_bytes")]
	etag: &'a [u8],

	last_modified: chrono::DateTime<chrono::Utc>,
	size: u64,
}

#[derive(Debug,serde::Deserialize,serde::Serialize)]
struct IpfsDir {
	#[serde(rename="Links")]
	links: Vec<IpfsLink>,
	#[serde(rename="Data")]
	data: String,
}

#[derive(Clone,Debug,Eq,PartialEq,Ord,PartialOrd,serde::Deserialize,serde::Serialize)]
struct IpfsLink {
	#[serde(rename="Hash")]
	hash: String,
	#[serde(rename="Name")]
	name: Option<String>,
	#[serde(rename="Size")]
	size: u64,
}

#[derive(Clone,Debug,Default,Eq,PartialEq)]
struct Item {
	ipfs: std::cell::RefCell<Option<std::rc::Rc<IpfsLink>>>,
	// Only valid if `children.is_some()`.
	is_directory: bool,
	children: Option<std::collections::HashMap<String, Item>>,
}

impl Item {
	fn from_file_hash(link: IpfsLink) -> Item {
		Item {
			ipfs: Some(link.into()).into(),
			is_directory: false,
			children: Some(Default::default()),
		}
	}

	fn get(&self, path: &str) -> Option<&Item> {
		let mut current = self;

		for name in path.split('/') {
			current = current.children.as_ref()?.get(name)?;
		}

		Some(current)
	}

	fn add(&mut self, path: &str, item: Item) {
		if self.try_add(path, item) {
			panic!("Duplicate item: {:?}", path);
		}
	}

	fn try_add(&mut self, path: &str, item: Item) -> bool {
		if self.children.is_none() {
			assert!(self.ipfs.borrow().is_none());
			self.children = Some(Default::default());
			self.is_directory = true;
		}
		self.ipfs = None.into();

		let mut iter = path.splitn(2, '/');
		let name = iter.next().unwrap();
		match iter.next() {
			Some(path) => {
				self.children.as_mut().unwrap().entry(name.into())
					.or_insert_with(Default::default)
					.try_add(path, item)
			}
			None => {
				use std::collections::hash_map::Entry;
				match self.children.as_mut().unwrap().entry(name.into()) {
					Entry::Occupied(_) => true,
					Entry::Vacant(e) => {
						e.insert(item);
						false
					}
				}
			}
		}
	}

	fn clone_ref(&self) -> Self {
		Item {
			ipfs: self.ipfs.clone(),
			is_directory: false,
			children: None,
		}
	}
}

fn encode_varint(mut i: u64, buf: &mut [u8; 10]) -> &mut [u8] {
	let mut o = 0;
	while i >= 128 {
		buf[o] = 0x80 | (i & 0x7F) as u8;
		i >>= 7;
		o += 1;
	}
	buf[o] = i as u8;
	o += 1;

	&mut buf[0..o]
}

struct Mirrorer {
	args: Args,
	base: String,
	http: reqwest::Client,
}

impl Mirrorer {
	fn init(&self) {
		std::fs::create_dir_all(self.args.flag_work_dir.join("http-cache")).unwrap();
	}

	async fn base_lastupdate(&self) -> i64 {
		let res = self.http.get("http://localhost:5001/api/v0/cat")
			.query(&[
				("arg", &format!("{}/latest/lastupdate", self.base)),
			])
			.send().await.unwrap();

		let status = res.status().as_u16();
		let body = res.text().await.unwrap();
		assert_eq!(status, 200, "Response: {}", body);
		body.trim().parse().unwrap()
	}

	async fn mirror_lastupdate(&self) -> i64 {
		let res = self.http.get(self.args.flag_mirror_url.join("lastupdate").unwrap())
			.send().await.unwrap();
		let status = res.status().as_u16();
		let body = res.text().await.unwrap();
		assert_eq!(status, 200, "Response: {}", body);
		body.trim().parse().unwrap()
	}

	fn list_files(&self) -> impl futures::Stream<Item=rsync_list::Entry> {
		let exclude = &[
			"*.links.tar.gz*".as_ref(),
			"/archive".as_ref(),
			"/other".as_ref(),
			"/sources".as_ref(),
			"/iso".as_ref(),

			// "*[1-3]*".as_ref(), // Reduce corpus for debugging.
		];

		let mut config = rsync_list::Config::new(&self.args.flag_mirror_rsync);
		config.exclude = exclude;
		config.executable = option_env!("RSYNC_PATH").unwrap_or("rsync").as_ref();

		config.list().map(|e| e.unwrap())
	}

	async fn ipfs_get(&self, cid: &str) -> Result<IpfsDir, String> {
		let res = self.http.get("http://localhost:5001/api/v0/object/get")
			.query(&[
				("arg", cid),
			])
			.send().await.unwrap();
		match res.status().as_u16() {
			200 => Ok(res.json().await.unwrap()),
			status => Err(format!(
				"Error object/get {} {}: {:?}",
				status,
				cid,
				res.text().await)),
		}
	}

	async fn ipfs_get_dir(&self, path: &str) -> Item {
		let mut dir = Item::default();
		dir.ipfs = Some(IpfsLink {
			hash: path.trim_start_matches("/ipfs/").into(),
			size: 0,
			name: None,
		}.into()).into();
		self.ipfs_set_children(&mut dir).await;
		dir
	}

	async fn ipfs_stat(&self, cid: &str) -> IpfsLink {
		#[derive(serde::Deserialize)]
		struct IpfsStat {
			#[serde(rename="Hash")]
			hash: String,
			#[serde(rename="CumulativeSize")]
			cumulative_size: u64,
		}

		let res = self.http.get("http://localhost:5001/api/v0/object/stat")
			.query(&[
				("arg", cid),
			])
			.send().await.unwrap();
		let res: IpfsStat = match res.status().as_u16() {
			200 => res.json().await.unwrap(),
			status => panic!(
				"Error object/stat {} {}: {:?}",
				status,
				cid,
				res.text().await),
		};

		IpfsLink {
			hash: res.hash,
			name: None,
			size: res.cumulative_size
		}
	}


	async fn ipfs_set_children(&self, dir: &mut Item) {
		if dir.children.is_some() {
			return
		}
		if dir.ipfs.borrow().is_none() {
			dir.children = Some(Default::default());
			return
		}

		let res = self.ipfs_get(&dir.ipfs.get_mut().as_ref().unwrap().hash)
			.await.unwrap();
		dir.is_directory = res.data == "\x08\x01";
		if dir.is_directory {
			dir.children = Some(
				res.links.into_iter()
					.map(|mut l| (
						std::mem::replace(&mut l.name, None).unwrap(),
						Item{
							ipfs: Some(l.into()).into(),
							is_directory: false,
							children: None,
						}))
					.collect());
		} else {
			dir.children = Some(Default::default());
		}
	}

	async fn ipfs_put(&self, links: impl IntoIterator<Item=IpfsLink>)
		-> Result<IpfsLink, String>
	{
		let links = links.into_iter();

		// Custom proto encoder!
		// https://github.com/ipfs/go-merkledag/blob/master/pb/merkledag.proto
		let mut buf = Vec::with_capacity(16 * links.size_hint().0);
		buf.extend(&[
			1 << 3 | 2, 2, // Data = 1 (bytes)
				// https://github.com/ipfs/go-unixfs/blob/master/pb/unixfs.proto
				8, // Type = 1 (varint)
				1, // Directory
		]);

		let mut total_size = 0;

		for link in links {
			total_size += link.size;

			buf.push(2 << 3 | 2); // Links = 2 (message)
			let link_start = buf.len();

			let mut cid = [0; 34];
			bs58::decode(&link.hash).into(&mut cid[..]).unwrap();

			buf.extend(&[1 << 3 | 2, cid.len() as u8]); // Hash = 1 (bytes)
			buf.extend(&cid[..]);

			let name = link.name.unwrap();
			assert!(name.len() < 128);
			buf.extend(&[2 << 3 | 2, name.len() as u8]); // Name = 2 (string)
			buf.extend(name.as_bytes());

			buf.push(3 << 3 | 0); // Name = 2 (uint64)

			let mut varint_buf = Default::default();
			buf.extend(encode_varint(link.size, &mut varint_buf).iter());

			buf.splice(
				link_start..link_start,
				encode_varint(
					(buf.len() - link_start).try_into().unwrap(),
					&mut varint_buf).iter().cloned());
		}

		let res = self.http.post("http://localhost:5001/api/v0/object/put")
			.query(&[
				("inputenc", "protobuf"),
			])
			.multipart(reqwest::multipart::Form::new()
				.part("file", reqwest::multipart::Part::bytes(buf)))
			.send().await.unwrap();
		match res.status().as_u16() {
			200 => {
				#[derive(serde::Deserialize)]
				struct Hash {
					#[serde(rename="Hash")]
					hash: String,
				}
				let res: Hash = res.json().await.unwrap();

				// This is racy but there is no other way to do this as we can't
				// directly add a non-recursive pin.
				let _ = self.ipfs_pin(&res.hash, false).await;

				Ok(IpfsLink{
					hash: res.hash,
					name: None,
					size: total_size,
				})
			}
			status => Err(format!(
				"Error object/put {}: {:?}",
				status,
				res.text().await)),
		}

	}

	async fn ipfs_pins_of(&self, typ: &str) -> impl Iterator<Item=String> {
		#[derive(serde::Deserialize)]
		struct Response {
			#[serde(rename="Keys")]
			keys: std::collections::HashMap<String, Info>,
		}

		#[derive(serde::Deserialize)]
		struct Info {}

		let res = self.http.get("http://localhost:5001/api/v0/pin/ls")
			.query(&[
				("type", typ),
			])
			.send().await.unwrap();
		let res: Response = match res.status().as_u16() {
			200 => res.json().await.unwrap(),
			status => panic!(
				"Error pin/ls {}: {:?}",
				status,
				res.text().await),
		};

		res.keys.into_iter().map(|(hash, _info)| hash)
	}

	async fn ipfs_pins(&self) -> impl Iterator<Item=String> {
		// We want everything but indirect, however fetching indirect pins is
		// very slow so we manually list all the other types.
		let (direct, recursive) = futures::join!(
			self.ipfs_pins_of("direct"),
			self.ipfs_pins_of("recursive"));

		direct.chain(recursive)
	}

	async fn ipfs_pin(&self, cid: &str, recursive: bool) -> Result<(), String> {
		let res = self.http.post("http://localhost:5001/api/v0/pin/add")
			.query(&[
				("arg", cid),
				("recursive", &format!("{}", recursive)),
			])
			.timeout(if recursive {
				// Recursive pins can take a long time, especially on spinning rust.
				std::time::Duration::from_secs(60 * 60)
			} else {
				std::time::Duration::from_secs(60)
			})
			.send().await.unwrap();
		match res.status().as_u16() {
			200 => Ok(()),
			status => {
				Err(format!(
					"Error pin/add {} {:?}: {:?}",
					status,
					cid,
					res.text().await))
			}
		}
	}

	async fn ipfs_unpin(&self, cid: &str) {
		let res = self.http.post("http://localhost:5001/api/v0/pin/rm")
			.query(&[
				("arg", cid),
			])
			// Removing large recursive pins can be slow.
			.timeout(std::time::Duration::from_secs(60 * 60))
			.send().await.unwrap();
		let status = res.status().as_u16();
		assert_eq!(status, 200,
			"Error pin/rm {} {:?}: {:?}",
			status,
			cid,
			res.text().await);
	}

	/// info: Overloaded parameter. If None, force a download. Otherwise listing
	/// information to avoid the request, and the cache will be used.
	async fn fetch_file(&self, path: &str, info: Option<File>) -> IpfsLink {
		let safe_path =
			percent_encoding::utf8_percent_encode(
				path, percent_encoding::NON_ALPHANUMERIC)
			.to_string();
		let cache_path = self.args.flag_work_dir.join("http-cache").join(&safe_path);

		let url = self.args.flag_mirror_url.join(path).unwrap();

		let timeout = std::time::Duration::from_secs(20 * 60);

		// eprintln!("GET {:?}", url);
		let mut req = self.http.get(url)
			.timeout(timeout);

		let cached_data;
		let cached = if let Some(info) = info {
			cached_data = tokio::fs::read(&cache_path).await;
			match &cached_data {
				Ok(data) => {
					match rmp_serde::from_read_ref(data) {
						Ok(entry) => {
							let entry: CacheEntry = entry;
							if entry.last_modified == info.last_modified
								&& entry.size == info.size
							{
								// eprintln!("Skipping due to Last-Modified + Size match.");
								return entry.link;
							}
							req = req.header(
								reqwest::header::IF_MODIFIED_SINCE,
								entry.last_modified.to_rfc2822());
							req = req.header(
								reqwest::header::IF_NONE_MATCH,
								entry.etag);
							Some(entry.link)
						}
						Err(e) => {
							eprintln!("Error reading {:?}: {:?}", cache_path, e);
							None
						}
					}
				}
				Err(_) => None,
			}
		} else {
			None
		};

		let start = std::time::Instant::now();
		let fetch_res = req.send().await.unwrap();

		let status = fetch_res.status().as_u16();
		let r = match status {
			200 => {
				let etag = fetch_res.headers()[reqwest::header::ETAG]
					.as_bytes().to_vec();
				let last_modified =
					chrono::DateTime::parse_from_rfc2822(
						fetch_res.headers()[reqwest::header::LAST_MODIFIED]
							.to_str().unwrap())
						.unwrap()
						.with_timezone(&chrono::Utc);
				let size = fetch_res.headers()[reqwest::header::CONTENT_LENGTH]
					.to_str().unwrap().parse().unwrap();

				let add_res = self.http.post("http://localhost:5001/api/v0/add")
					.query(&[
						("chunker", "rabin"),
						("pin", "true"),
					])
					.multipart(reqwest::multipart::Form::new()
						.part("file", reqwest::multipart::Part::stream(fetch_res)))
					.timeout(timeout)
					.send().await.unwrap();
				assert_eq!(add_res.status().as_u16(), 200);

				// Add returns stringified numbers but object/put doesn't accept stringified numbers 🤦
				let mut link: serde_json::Value = add_res.json().await.unwrap();
				let ipfs_size: u64 = link["Size"].as_str().unwrap().parse().unwrap();
				link["Size"] = ipfs_size.into();
				let text = serde_json::to_vec(&link).unwrap();
				let link: IpfsLink = serde_json::from_slice(&text).unwrap();

				let buf = rmp_serde::to_vec_named(&CacheEntry {
					link: link.clone(),
					etag: &etag,
					last_modified,
					size,
				}).unwrap();
				tokio::fs::write(&cache_path, buf).await.unwrap();

				link
			}
			304 => {
				cached.expect("304 on non-conditional request.")
			}
			other => panic!("Unexpected HTTP status {} for {:?}", other, path),
		};

		let end = std::time::Instant::now();

		eprintln!("{} in {:?} {:?} {}", status, end - start, path, r.hash);

		r
	}

	fn save(&self,
		dir: &Item,
		mut pins: Option<&mut std::collections::HashSet<String>>
	) -> std::rc::Rc<IpfsLink> {
		let mut ipfs = dir.ipfs.borrow_mut();
		if let Some(ref link) = &*ipfs { return link.clone() }

		let mut links = dir.children.as_ref().unwrap().iter()
			.map(|(name, item)| {
				let mut link = (*self.save(item, pins.as_deref_mut())).clone();
				link.name = Some(name.into());
				link
			})
			.collect::<Vec<_>>();
		links.sort_unstable();

		let link = async_std::task::block_on(self.ipfs_put(links)).unwrap();

		if let Some(ref mut set) = pins {
			set.insert(link.hash.to_string());
		}

		let link = std::rc::Rc::new(link);

		*ipfs = Some(link.clone());

		link
	}

	async fn merge(&self, left: &str, right: &mut Item) {
		let right_link = self.save(right, None);

		#[derive(serde::Deserialize)]
		struct Response {
			#[serde(rename="Changes")]
			changes: Vec<Diff>,
		}

		#[derive(serde::Deserialize)]
		struct Diff {
			#[serde(rename="Type")]
			type_: u8,
			#[serde(rename="Path")]
			path: String,
			#[serde(rename="Before")]
			before: Option<Link>,
			// #[serde(rename="After")]
			// after: Option<Link>,
		}

		#[derive(serde::Deserialize)]
		struct Link {
			#[serde(rename="/")]
			hash: String,
		}

		let res = self.http.get("http://localhost:5001/api/v0/object/diff")
			.query(&[
				("arg", left),
				("arg", &right_link.hash),
			])
			.send().await.unwrap();

		let res: Response = match res.status().as_u16() {
			200 => res.json().await.unwrap(),
			status => panic!(
				"Error object/diff {} {} {}: {:?}",
				status,
				left,
				right_link.hash,
				res.text().await),
		};

		let iter = res.changes.into_iter()
			.filter(|diff| diff.type_ == 1)
			.dedup_by(|a, b| a.path == b.path);
		let mut stream = futures::stream::iter(iter)
			// https://github.com/ipfs/go-merkledag/blob/d532cda6aaa6b68e23b1876cddecf039e5ecc6ab/dagutils/diff.go#L19
			// Remove
			.map(|diff| async {
				(diff.path, self.ipfs_stat(&diff.before.unwrap().hash).await)
			})
			.buffer_unordered(4);

		while let Some((path, link)) = stream.next().await {
			// We use try_add because object/diff diffs blocks. This means that it may report a removal if the file was truncated. This means that we may have spurious removals for files that exist on the right-hand side. See https://github.com/ipfs/go-ipfs/issues/7044 for details.
			right.try_add(&path, Item::from_file_hash(link));
		}
	}

	fn snapshot_name(&self, date: chrono::DateTime::<chrono::Utc>) -> String {
		format!("{}", date.format("%FT%R:%S"))
	}

	async fn mirror(&self) {
		let (
			base_lastupdate,
			mirror_lastupdate,
		) = futures::join!(
			self.base_lastupdate(),
			self.mirror_lastupdate());

		let date =
			chrono::DateTime::<chrono::Utc>::from_utc(
				chrono::NaiveDateTime::from_timestamp(mirror_lastupdate, 0),
				chrono::Utc);
		let snapshot_name = self.snapshot_name(date);

		if base_lastupdate == mirror_lastupdate {
			eprintln!("Up to date: {}", snapshot_name);
			return
		} else if base_lastupdate > mirror_lastupdate {
			eprintln!("Mirror behind base: {} vs {}",
				base_lastupdate,
				mirror_lastupdate);
			return
		}

		if !self.args.flag_debug {
			tokio::time::delay_for(std::time::Duration::from_secs(60)).await;
		}

		let mut resolver = std::collections::HashMap::<String, Vec<String>>::with_capacity(1000);

		let mut files = Item::default();
		let listing = self.list_files();
		pin_utils::pin_mut!(listing);
		while let Some(entry) = listing.next().await {
			let path = std::str::from_utf8(entry.path()).unwrap();

			if entry.path() == b"." { continue }

			match entry.kind {
				rsync_list::Kind::Dir(_) => {}
				rsync_list::Kind::File(_) => {
					assert!(files.get(path).is_none(),
						"File {:?} listed twice.", path);

					let stat = Some(File {
						last_modified: entry.last_modified(),
						size: entry.size(),
					});
					let link = self.fetch_file(&path, stat).await;
					files.add(&path, Item::from_file_hash(link.clone()));
					if let Some(links) = resolver.remove(path) {
						for path in links {
							files.add(&path, Item::from_file_hash(link.clone()));
						}
					}
				}
				rsync_list::Kind::Link(ref link) => {
					// This assumes that the symlink is somewhat normalized.
					// However if it isn't the only downside should be using
					// funny URLs and possibly missing caches.
					let mut target = std::str::from_utf8(link.target()).unwrap();
					let mut resolved: String = path.into();
					while resolved.pop().unwrap_or('/') != '/' {}
					while target.starts_with("../") {
						assert!(!resolved.is_empty(),
							"Unsafe link {:?} -> {:?}", path, target);

						target = &target[3..];
						while resolved.pop().unwrap_or('/') != '/' {}
					}
					if !resolved.is_empty() {
						resolved.push('/');
					}
					resolved.push_str(target);

					if let Some(link) = files.get(&resolved) {
						let link = link.clone();
						files.add(&path, link);
					} else {
						resolver.entry(resolved)
							.or_insert_with(Vec::new)
							.push(path.into());
					}

				}
				_ => panic!("Unknown entry type {:?}", entry),
			}
		}

		assert!(resolver.is_empty(), "Unresolved links: {:?}", resolver);

		if !self.args.flag_debug {
			tokio::time::delay_for(std::time::Duration::from_secs(60)).await;
		}

		let end_lastupdate = self.mirror_lastupdate().await;
		if end_lastupdate != mirror_lastupdate {
			eprintln!("Possible version skew detected {:?} -> {:?}",
				mirror_lastupdate, end_lastupdate);
			std::process::exit(2);
		}

		let mut pins = std::collections::HashSet::new();

		let latest = self.save(&files, None);
		eprintln!("Latest is /ipfs/{}", latest.hash);

		self.ipfs_pin(&latest.hash, true).await.unwrap();
		pins.insert(latest.hash.clone());

		let mut snapshots = self.ipfs_get_dir(
			&format!("{}/snapshots", self.base)).await;
		snapshots.add(&snapshot_name, files.clone_ref());

		if self.args.flag_pin_count != 0 {
			let cutoff_date = self.snapshot_name(
				self.args.flag_pin_time
					.map(|duration| date - chrono::Duration::from_std(duration).unwrap())
					.unwrap_or(std::time::UNIX_EPOCH.into()));


			let past_snapshots = snapshots.children
				.as_ref()
				.unwrap()
				.iter()
				.sorted_by_key(|pair| pair.0)
				.collect::<Vec<_>>();
			for (i, &(snapshot, entry)) in past_snapshots.iter().rev().enumerate() {
				let too_many = i >= self.args.flag_pin_count;
				let too_old = snapshot < &cutoff_date;
				if too_many && too_old {
					break
				}
				pins.insert(self.save(entry, None).hash.clone());
			}
		} else {
			assert_eq!(self.args.flag_pin_time, None,
				"--pin-time requires a non-zero --pin-count");
		}
		
		let mut root = Item::default();
		root.add("latest", files.clone_ref());
		root.add("snapshots", snapshots);

		let start = std::time::Instant::now();
		self.merge(&format!("{}/all", &self.base), &mut files).await;
		root.add("all", files);
		eprintln!("merged in {:?}", std::time::Instant::now() - start);

		let start = std::time::Instant::now();
		let mirror = self.save(&root, Some(&mut pins));
		eprintln!("saved in {:?}", std::time::Instant::now() - start);
		self.ipfs_pin(&mirror.hash, false).await.unwrap();
		let mirror = format!("/ipfs/{}", mirror.hash);
		eprintln!("New mirror is {}", mirror);

		let res = self.http.post("http://localhost:5001/api/v0/name/publish")
			.query(&[
				("arg", mirror.as_str()),
				("allow-offline", "true"),
				("key", &self.args.flag_ipns_key),
				("lifetime", "720h"),
				("ttl", "10m"),
			])
			.send()
			.await.unwrap();
		assert_eq!(res.status().as_u16(), 200);

		eprintln!("Published.");

		let start = std::time::Instant::now();
		let stale_pins = self.ipfs_pins().await
			.filter(|cid| !pins.contains(cid));
		futures::stream::iter(stale_pins)
			.map(|cid| async move { self.ipfs_unpin(&cid).await})
			.buffer_unordered(2)
			.collect::<()>().await;
		eprintln!("cleaned pins in {:?}", std::time::Instant::now() - start);
	}
}

#[tokio::main]
async fn main() {
	let args: Args = docopt::Docopt::new(USAGE)
		.and_then(|d| d.deserialize())
		.unwrap_or_else(|e| e.exit());

	let http = reqwest::ClientBuilder::new()
		.user_agent("archlinux-ipfs-mirror/0")
		.connect_timeout(std::time::Duration::from_secs(60))
		.timeout(std::time::Duration::from_secs(10 * 60))
		.redirect(reqwest::redirect::Policy::none())
		.build().unwrap();

	#[derive(serde::Deserialize)]
	struct ResolveResponse {
		#[serde(rename="Path")]
		path: String,
	}

	let base = if args.flag_base.starts_with("/ipns/") {
		let res = http.get("http://localhost:5001/api/v0/name/resolve")
			.query(&[
				("arg", args.flag_base.as_str()),
			])
			.send().await.unwrap();
		assert_eq!(res.status().as_u16(), 200,
			"Could not resolve {:?}: {:?}", args.flag_base, res.text().await);
		let res: ResolveResponse = res.json().await.unwrap();
		res.path
	} else {
		args.flag_base.clone()
	};

	eprintln!("Base: {}", base);

	let m = Mirrorer {
		args,
		base,
		http,
	};
	m.init();
	m.mirror().await;

	eprintln!("Done");
}
